# One File Projects #
This repo contains interesting ( my opinion ) files, some of them can even be useful

**NumberDecomposition**  - returns decomposition of the given number (should return 5,2 for 10, because 10 = 5*2)

 **Registration** - registers and then confirms about 600 accounts on the garant.ru using grab lib and much, much hardcode. Can be useful though.

  **Snake**  - An implementation of the shake game, using win API.

  **new_main.py** - An extension to registration file, downloads docs from garant, using heavy artillery in face of selenium web driver. Can also be usefull as it contains an example of parsing heavy js sites, when full browser implementation is needed in order to download required file.