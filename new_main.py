from selenium import webdriver
import time

LOG_FILE = open("log.txt", "w")
LINKS_FILE = open("links.txt", "w")

WAITING_TIME = 1
DOWNLOAD_TIME = 2

TRY_COUNT = 10

def Login(name, driver):
    base_url = "http://www.garant.ru/"
    driver.maximize_window()
    driver.get(base_url)
    for i in range(10):
        try:
            driver.find_element_by_link_text(u"Войти").click()
            driver.find_element_by_id("pers_login").clear()
            driver.find_element_by_id("pers_login").send_keys(name)
            driver.find_element_by_id("pers_password").clear()
            driver.find_element_by_id("pers_password").send_keys("Mypas2")
            driver.find_element_by_id("register").click()
            break
        except:
            time.sleep(WAITING_TIME)

def ExtractId(doc_link):
    index = doc_link[22:].find('/')
    doc_link = doc_link[22:22 + index]
    return doc_link

def Order(doc_link, driver):
    error_occured = False

    for i in range(TRY_COUNT):
        try:
            driver.get(doc_link)
            break
        except:
            time.sleep(WAITING_TIME)
            if i == TRY_COUNT - 1:
                error_occured = True
                LOG_FILE.write("Failed to load: " + doc_link)
                return

def ProccesOrder(doc_link, driver):
    error_occured = False
    for i in range(TRY_COUNT):
        try:
            driver.find_element_by_link_text(u"Вы можете заказать текст документа и получить его бесплатно прямо сейчас.").click()
            break
        except:
            if i == TRY_COUNT - 1:
                error_occured = True
                LOG_FILE.write("failed to Process")
                LINKS_FILE.write(doc_link + '\n')
                return True
            else:
                time.sleep(WAITING_TIME)

    if error_occured == False:
        time.sleep(WAITING_TIME)
        try:
            driver.switch_to.alert.accept()
            driver.switch_to.window(driver.window_handles[1])
            driver.find_element_by_name("submit").click()
        except:
            LOG_FILE.write("failed to Process")
    return False


def DownloadDoc(doc_id, number, driver):
    link = "http://www.garant.ru/personal/ivlist/?id=" + doc_id
    for i in range(TRY_COUNT):
        try:
            driver.get(link)
            break
        except:
            time.sleep(WAITING_TIME)
    try:
        driver.find_element_by_xpath('//*[@id="page"]/section/div/div/div[2]/div[5]/table[' + str(number+1) + ']/tbody/tr[2]/td[1]/a').click()
    except:
        LOG_FILE.write("UNABLE TO LOCATE ELEMENT:" + link)
        LINKS_FILE.write(link + '\n')
        return
    #//*[@id="page"]/section/div/div/div[2]/div[5]/table[1]/tbody/tr[2]/td[1]/a
    #//*[@id="page"]/section/div/div/div[2]/div[5]/table[2]/tbody/tr[2]/td[1]/a
    for i in range(TRY_COUNT):
        try:
            driver.switch_to.frame("utMainWindow")
            driver.switch_to.frame("frame24_utMainWindow")
            driver.find_element_by_css_selector(u"img[alt=\"Экспорт в MS-Word\"]").click()
            break
        except:
            if i == TRY_COUNT - 1:
                LOG_FILE.write("HTML? " + link)
                LINKS_FILE.write(link)
            else:
                time.sleep(WAITING_TIME)
    time.sleep(DOWNLOAD_TIME)

def ClosingWindows(driver):
        driver.quit()

def MainLoop():
    email_file = open("emails.txt", "r")
    full_links = open("full_links.txt", "r")
    while(1):
        email = email_file.readline()
        for i in range(3):
            profile = webdriver.FirefoxProfile('/home/mikhail/.mozilla/firefox/qi4n0za5.default')
            driver = webdriver.Firefox(profile)
            Login(email, driver)
            doc_link = full_links.readline()
            if doc_link == "":
                print("FINISHED")
                return
            doc_id = ExtractId(doc_link)
            Order(doc_link, driver)
            if ProccesOrder(doc_link, driver) == False:
                time.sleep(WAITING_TIME)
                DownloadDoc(doc_id, i, driver)
            else:
                i -= 1
            ClosingWindows(driver)


MainLoop()







