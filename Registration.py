WAITING_TIME = 10
AFTER_ERROR_SLEEP_TIME = 120
NUMBER_OF_EMAILS_TO_COLLECT = 600

def CollectingEmails():
    for i in range(NUMBER_OF_EMAILS_TO_COLLECT):
        try:
            file = open('emails.txt', 'a')
            g = Grab(log_file='out.html')
            g.go('http://www.incognitomail.com//')
            g.doc.choose_form(0)
            g.doc.submit()
            email = g.doc.select('//div[contains(@id, "t12l_body")]')[0].select('//div[contains(@id, "temporary_address")]')[0].html()[161:187]
            print("ATTEMPT №" + " " + str(i) + " " + email)
            RegisterNewOne(email)
            time.sleep(WAITING_TIME)
            
            g.go('http://www.incognitomail.com//')
            ref = g.doc.select('//*[@id="email_message_list"]').html()[120:130]
            g.go('http://www.incognitomail.com/' + ref)
            
            i = g.doc.select('//div[contains(@id, "t12l_body")]').text().find("http://www.garant.ru/personal/register/confirm") 
            j = g.doc.select('//div[contains(@id, "t12l_body")]').text().find(" Вы можете ввести код")
            confirm_ref = g.doc.select('//div[contains(@id, "t12l_body")]').text()[i:j]

            ConfirmReg(confirm_ref)
            file.write(email + "\n")
            file.close()
        except:
            time.sleep(AFTER_ERROR_SLEEP_TIME)
            print("ERROR")

    return g
